var fs = require('fs');

var data = fs.readFileSync(__dirname + '/accession.json');
var accessionIds = JSON.parse(data);
module.exports = {

    validChars: "ABCDEFGHIKLMNPQRSTVWXYZ",
    /**
     * checks if a given protein sequence is valid based on a basic syntax.
     * Case insensitive.
     */
    isValidSequence: function (sequence) {
        if(!sequence){
            return false;
        }
        
        seq = sequence.toUpperCase();
        seq = seq.split("");

        for (var i = 0; i < seq.length; i++) {
            if(!this.isValidChar(seq[i])){
                return false;
            }
        }

        return true;
    },

    isValidChar: function (character) {
        if (this.validChars.indexOf(character) === -1) {
            return false;
        }
        
        return true;
    },

    getSequenceFromAccessionID: function (accessionId) {
        var entry = accessionIds[accessionId];

        if (!entry) {
            throw new Error("Accession Id did not return a match!");
        }

        return accessionIds[accessionId];
    }
}