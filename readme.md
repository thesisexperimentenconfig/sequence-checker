# Usage
`isValidSequence(String): Boolean` checks whether the given string has the valid syntax for a sequence.
`getSequenceFromAccessionId(String): String`: returns a sequence based on the accession id. Throws an error if the accession id is not found.
#Tests
After installing
    npm install
    npm install -g istanbul

run the command
    npm test
	
in terminal.