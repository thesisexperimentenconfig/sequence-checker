var checker = require('../index.js');

var should = require('chai').should();
var expect = require('chai').expect;

describe('Test sequence checker', function(){
	
	it('test #isValidSequence(): valid cases', function(){
		var result = checker.isValidSequence('IKFAPDELVYMMIANDEHAN');
		result.should.eql(true);
		
		result = checker.isValidSequence('IKFAPDedLVYMMIANDEHAN');
		result.should.eql(true);
	});
	
	it('test #isValidSequence(): invalid cases', function(){
		var result = checker.isValidSequence('IKFJJAPDELVYMMIANDEHAN');
		result.should.eql(false);
		
		result = checker.isValidSequence('IKFAPDffjjedLVYMMIANDEHAN');
		result.should.eql(false);
		
		result = checker.isValidSequence('');
		result.should.eql(false);
	});
	
	it('#getSequenceFromAccessionID()', function(){
		var result = checker.getSequenceFromAccessionID('P31946');
		result.should.eql('MTMDKSELVQKAKLAEQAERYDDMAAAMKAVTEQGHELSNEERNLLSVAYKNVVG' +
			'ARRSSWRVISSIEQKTERNEKKQQMGKEYREKIEAELQDICNDVLELLDKYLIPNATQPESKVFYLKMKGDY' +
			'FRYLSEVASGDNKQTTVSNSQQAYQEAFEISKKEMQPTHPIRLGLALNFSVFYYEILNSPEKACSLAKTA' +
			'FDEAIAELDTLNEESYKDSTLIMQLLRDNLTLWTSENQGDEGDAGEGEN')
		
		result = checker.getSequenceFromAccessionID('Q96QU6');
		result.should.eql('MFTLPQKDFRAPTTCLGPTCMQDLGSSHGEDLEGECSRKLDQKLPELRGVGDPAMISSDTSYLSSRGRMIKWFW' +
		'DSAEEGYRTYHMDEYDEDKNPSGIINLGTSENKLCFDLLSWRLSQRDMQRVEPSLLQYADWRGHLFLREEVAKFL'+
		'SFYCKSPVPLRPENVVVLNGGASLFSALATVLCEAGEAFLIPTPYYGAITQHVCLYGNIRLAYVYLDSEVTGLDTR'+
		'PFQLTVEKLEMALREAHSEGVKVKGLILISPQNPLGDVYSPEELQEYLVFAKRHRLHVIVDEVYMLSVFEKSVGYRSV'+
		'LSLERLPDPQRTHVMWATSKDFGMSGLRFGTLYTENQDVATAVASLCRYHGLSGLVQYQMAQLLRDRDWINQVYLPENHA'+
		'RLKAAHTYVSEELRALGIPFLSRGAGFFIWVDLRKYLPKGTFEEEMLLWRRFLDNKVLLSFGKAFECKEPGWFRFVFSDQVHR'+
		'LCLGMQRVQQVLAGKSQVAEDPRPSQSQEPSDQRR');
		
		
		expect(function(){
			checker.getSequenceFromAccessionID('');
		}).to.throw;
		
		expect(function(){
			checker.getSequenceFromAccessionID('dsq');
		}).to.throw;
	});
	
	
});